# Lyfer - Lyft vs Uber

Lyfer is an innovative app to compare prices from Lyft and Uber for Multi destination round trips planning. 

### Setup Instructions
  - Clone the repository https://github.com/arpitdesai12/Uber-vs-Lyft-price-estimator
  - run with "python UberVsLyft.py"
  - Open http://localhost:3000 or with IP of your machine with port 3000
\
 

### Contributors
  - Neha Kumar
  - Arpeet Desai
  - Purvil Kamdar
  - Ritvick Paliwal

